import React, { Component } from 'react';

import AddUser from './AddUser';
import './App.css';
import {
  BrowserRouter as Router,
  Switch
} from 'react-router-dom';
import Home from './router/Home';
import Deliverables from './router/Deliverables';
import CustomRoute from './router/CustomRoute';
import ProfMenu from './router/ProfMenu';
import GradeProject from './router/GradeProject';



class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: {}
    };
  }

  render() {
    return (
      <div className="App">
         <Router>
          <Switch>
            <CustomRoute exact path='/gradeproject' Component={GradeProject} />
            <CustomRoute exact path="/profmenu" Component={ProfMenu} />
            <CustomRoute exact path="/deliverables" Component={Deliverables} />
            <CustomRoute exact path="/home" Component={Home} />
            <CustomRoute exact path="/" Component={AddUser} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
