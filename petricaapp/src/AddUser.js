import React from 'react';
import axios from 'axios';
var localhost = "18.216.139.124";

class AddUser extends React.Component{
  constructor(props) {
      super(props);
      this.state = {
            username: '',
            password: '',
            user: {}
        };
    }

    handleChangeUsername= (event) => {
        this.setState({
            username: event.target.value
        });
    }
    
    handleChangePassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }
    
    loginToDB = () => {
      axios.get(`http://${localhost}:8080/users/` + this.state.username).then(users => {
            this.setState({
             user: users.data
      })
      if (users.data.parola === this.state.password){
                console.log("Logare reusita");
                console.log(this.state.user);
                var successLogin = document.getElementById('loginSuccessID');
                successLogin.style.visibility = "visible";
                var mainAppButton = document.getElementById('mainAppButtonID');
                mainAppButton.style.visibility = "visible";
      }
      else{
          alert("Parola gresita!");
      }
    }).catch(function(err){
          if (err.response.status === 404){
              alert("Email gresit!"); 
          }
      })
        
    }
    
    goToMainApp = () => {
        this.props.history.push(`/home`, { user: this.state.user });
    }

    render() {
        return (
            <div>
            <h1 className="welcomeHeader">Bine ati venit!</h1>
            <input
                type="text"
                placeholder="Email"
                value={this.state.username}
                onChange={this.handleChangeUsername}
            />
            <input
                type="password"
                placeholder="Parola"
                value={this.state.password}
                onChange={this.handleChangePassword}
                />
                <button
                className="loginButton"
                onClick={this.loginToDB}>
                Login</button>
                
            <h1 id="loginSuccessID" className="loginSuccess">Logare reusita! Continuati spre </h1>
            <button id="mainAppButtonID" className="mainAppButton" onClick={this.goToMainApp}>Home</button>
        
            </div>
            );
    }
}

export default AddUser;