import React from 'react';
import axios from 'axios';
var localhost = "18.216.139.124";

class ProfMenu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentUser: this.props.location.state.user,
            grading: this.getGrading(),
            currentProject: ''
        };
    }
    
    getGrading = () => {
        axios.get(`http://${localhost}:8080/grading/1`).then(gradingData => {
            this.setState({
            grading: gradingData.data
        }); 
    });
    }
    
    
    changeGrading = () => {
        axios.put(`http://${localhost}:8080/grading/1`, {gradingActiv: this.state.grading.gradingActiv});
    }
    
    
    startGrading = () => {
        this.getGrading();
        console.log(this.state.grading);
        if (this.state.grading.gradingActiv === "false"){
            // eslint-disable-next-line
            this.state.grading.gradingActiv = "true";
            alert("Procesul de notare a inceput!");
            this.changeGrading();
        }
        else{
            alert("Procesul de notare este deja inceput!");
            console.log(this.state.grading);
        }
    }
    
  
    stopGrading = () => {
        this.getGrading();
        console.log(this.state.grading);
         if (this.state.grading.gradingActiv === "true"){
             // eslint-disable-next-line
            this.state.grading.gradingActiv = "false";
            alert("Procesul de notare s-a incheiat!");
            this.changeGrading();
            console.log(this.state.grading);
        }
        else{
            alert("Procesul de notare este deja incheiat!");
            console.log(this.state.grading);
        }
    }
    
    goToHome = () => {
        this.props.history.push('/home', {user: this.state.currentUser});
    }
    
    handleChangeProject = (event) => {
        this.setState({
           currentProject: event.target.value 
        });
    }
    
    checkGrades = () => {
        var lista = document.getElementById('gradeList');
        axios.get(`http://${localhost}:8080/users`).then(users => {
            var utilizatori = users.data;
            var count = 0;
            var total = 0;
            var numar = 0;
            var min = 0;
            var max = 11;
            while (utilizatori[count] !== undefined){
                
                if (utilizatori[count].id_proiect_notat == this.state.currentProject){
                    if (utilizatori[count].nota_acordata){
                         if (utilizatori[count].nota_acordata > min){
                        min = utilizatori[count].nota_acordata;
                     }
                          if (utilizatori[count].nota_acordata < max){
                        max = utilizatori[count].nota_acordata;
                    }
                         total = total + utilizatori[count].nota_acordata;
                        var entry = document.createElement('li');
                         entry.appendChild(document.createTextNode(utilizatori[count].nota_acordata));
                        lista.appendChild(entry);
                         numar ++;
                         count ++;
                    }
               else{
                count ++;
            }
            }
                else{
                count ++;
            }
        }
            total = total - min - max;
            numar = numar - 2;
            var medie = total / numar;
            if (numar == -2 ){
                alert('Acest proiect nu s-a notat inca!');
            }
            else{
                alert(`Ignorand valorile extreme, media este de ${medie}`);
            }
            
            
    });
    }
    
    render() {
        return(
            <div>
                <h1 className="welcomeHeader">Meniu Profesor</h1>
                <div className="UserRowHome">
                    <p>User Curent: </p>
                    <input 
                        className="inputReadOnly"
                        type="text"
                        value={this.state.currentUser.nume_utilizator}
                        readOnly/>
                </div>
                
                <h1 id="gradingHeaderID" className="gradingHeader">Optiuni notare</h1>
                
                <button id="gradingStartID" className="gradingStartButton" onClick={this.startGrading}>Start</button>
                <button id="gradingStopID" className="gradingStopButton" onClick={this.stopGrading}>Stop</button>
                
                <div className="UserProjHome">
                    <p>Proiect Verificat: </p>
                    <input
                        className="inputReadWrite"
                        type="text"
                        value={this.state.currentProject}
                        onChange={this.handleChangeProject}
                        />
                </div>
                <button id="gradingCheckID" className="gradingCheckButton" onClick={this.checkGrades}>Verifica notarea</button>
                <ol className="olList" id="gradeList"></ol>
                
                <button id="gradingBackID" className="gradingBackButton" onClick={this.goToHome}>Inapoi</button>
                
            </div>
            );
    }
}

export default ProfMenu;