import React from 'react';
import axios from 'axios';
var localhost = "18.216.139.124";

class Home extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentUser: this.props.location.state.user,
            grading: this.getGrading()
        };
    }
    
    goToDeliv = () => {
        this.props.history.push('/deliverables', {id_proiect: this.state.currentUser.id_proiect, user: this.state.currentUser});
    }
    
    getGrading = () => {
        axios.get(`http://${localhost}:8080/grading/1`).then(gradingData => {
            this.setState({
            grading: gradingData.data
        }); 
    });
    
    }
    changeGrading = () => {
        axios.put(`http://${localhost}:8080/grading/1`, {grading: "true"});
    }
        

    showMenus = () => {
        this.getGrading();
        console.log(this.state.grading);
        var delivButtonID = document.getElementById('delivButtonID');
        var profMenuID = document.getElementById('profMenuID');
        var notatiButonID = document.getElementById('gradeProjectID');
        if (this.state.currentUser.rol === "student"){
            delivButtonID.style.display = "block";
            profMenuID.style.display = "none";
            if(this.state.grading.gradingActiv === "true"){
                alert("Notarea este activa!");
                delivButtonID.style.display = "none";
                notatiButonID.style.display = "block";
            }
            else{
                alert("Notarea nu este activa!");
            }
        }
        else{
            delivButtonID.style.display = "none";
            profMenuID.style.display = "block";
            notatiButonID.style.display = "none";
        }
    }
    
    goToLogin = () => {
        this.props.history.push('/');
    }
    
    goToProfMenu = () => {
        this.props.history.push('/profmenu',  {user: this.state.currentUser} )
    }
    
    goToGrade = () => {
        this.props.history.push('/gradeproject', {user: this.state.currentUser})
    }

    render() {
        return(
            <div>
                <h1 className="welcomeHeader">Acasa</h1>
                <div className="UserRowHome">
                    <p>User Curent: </p>
                    <input 
                        className="inputReadOnly"
                        type="text"
                        value={this.state.currentUser.nume_utilizator}
                        readOnly/>
                </div>
                <div className="UserProjHome">
                    <p>Proiect Curent: </p>
                    <input
                        className="inputReadOnly"
                        type="text"
                        value={this.state.currentUser.id_proiect}
                        readOnly/>
                </div>
                <button id="showmenusID" className="showMenusButton" onClick={this.showMenus}>Arata meniu</button>
                <button id="backToLoginID" className="backToLoginButton" onClick={this.goToLogin}>Inapoi</button>
                <button id="delivButtonID" className="homeAppButton" onClick={this.goToDeliv}>Livrabile</button>
                <button id="profMenuID" className="profMenuButton" onClick={this.goToProfMenu}>Meniu Profesor</button>
                
                <button id="gradeProjectID" className="gradeProjButton" onClick={this.goToGrade}>Notare</button>
            </div>
            
            
            );
    }
}

export default Home;