import React from 'react';
import axios from 'axios';
var localhost = "18.216.139.124";
var nr = 1;
var nrCurent = 1;
var today;

class Deliverables extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            link: '',
            user: this.props.location.state.user,
            id_proiect: this.props.location.state.id_proiect,
            allDeliverables: {}
        };
    }
    
    handleChangeLink = (event) => {
        this.setState({
           link: event.target.value 
        });
    }
    
    getFromDatabase = () => {
        axios.get(`http://${localhost}:8080/deliverables/${nr}`).then(populateAll => {
            this.setState({
            allDeliverables: populateAll.data
        }); 
        var lista = document.getElementById('delivList');
        if (this.state.allDeliverables.id_livrabil != null){
               if (this.state.allDeliverables.id_proiect === this.state.id_proiect){
                var entry = document.createElement('li');
                entry.appendChild(document.createTextNode(this.state.allDeliverables.share));
                lista.appendChild(entry);
               nr++;
               nrCurent++;
               this.getFromDatabase();
               }
               else{
                   nr++;
                   this.getFromDatabase();
               }
           }
        });
    }
    
    addToDatabase = () => {
        axios.post(`http://${localhost}:8080/deliverables`, {
            id_proiect: this.state.id_proiect,
            nr_livrabil: nrCurent-1,
            share: this.state.link,
            data_livrare: today
        })
    }
    
    addDelivToList = () => {
        today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0');
        var yyyy = today.getFullYear();
        today = mm + '-' + dd + '-' + yyyy;
        this.addToDatabase();
        nr = 1;
        nrCurent = 1;
        this.getFromDatabase();
    }
    
    seeDelivList = () => {
        if (document.querySelector('li')==null){
            nr = 1;
            nrCurent = 1;
            this.getFromDatabase();
        }
    }
    
    goBackDeliv = () => {
        this.props.history.push('/home', {user: this.state.user});
    }
    
    render() {
        return(
            <div>
                <h1 className="welcomeHeader">Livrabile</h1>
                <div className="UserRowHome">
                    <p>Link: </p>
                    <input 
                        className="inputReadWrite"
                        placeholder="Link pentru livrabil"
                        type="text"
                        value={this.state.link}
                        onChange={this.handleChangeLink}
                        />
                </div>
                <button id="addDelivButtonID" className="delivAppButton" onClick={this.addDelivToList}>Adauga</button>
                <button id="seeDelivListButtonID" className="delivShowButton" onClick={this.seeDelivList}>Lista Livrabile</button>
                <ol className="olList" id="delivList"></ol>
                <button id="backDelivID" className="backDelivButton" onClick={this.goBackDeliv}>Inapoi</button>
            </div>
            );
    }
}

export default Deliverables;