import React from 'react';
import {Route} from 'react-router-dom';

class CustomRoute extends React.Component{
    render() {
        const { Component, ...rest } = this.props;
        return (
                <Route {...rest} render={(props) => (
                        <Component {...props} />
                )} />
            );
    }
}


export default CustomRoute;