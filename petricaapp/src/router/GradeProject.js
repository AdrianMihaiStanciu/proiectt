import React from 'react';
import axios from 'axios';
var localhost = "18.216.139.124";
var nr = 1;

class GradeProject extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            user: this.props.location.state.user,
            nota: this.props.location.state.user.nota_acordata,
            allDeliverables: {}
        };
    }
    
    getProjectToRate = () => {
       axios.get(`http://${localhost}:8080/projects/${nr}`).then(project => {
            nr++;
            this.getProjectToRate();
    });
    }
    
    assignProject = () => {
       // this.getProjectToRate();
       // var random = Math.floor(Math.random() * nr) + 1;
       // while (random===this.state.user.id_proiect){
       //     random = Math.floor(Math.random() * nr) + 1;
       // }
       // axios.put(`http://${localhost}:8080/users/${this.state.user.email}`, {id_proiect_notat: random});
       // console.log(this.state.user);
       axios.get(`http://${localhost}:8080/deliverables/${nr}`).then(populateAll => {
            this.setState({
            allDeliverables: populateAll.data
        }); 
        var lista = document.getElementById('delivListGrade');
        if (this.state.allDeliverables.id_livrabil != null){
               if (this.state.allDeliverables.id_proiect === this.state.user.id_proiect_notat){
                var entry = document.createElement('li');
                entry.appendChild(document.createTextNode(this.state.allDeliverables.share));
                lista.appendChild(entry);
               nr++;
               this.assignProject();
               }
               else{
                   nr++;
                   this.assignProject();
               }
           }
        });
    }
    
    seeDelivList = () => {
        if (document.querySelector('li')==null){
            nr = 1;
            this.assignProject();
        }
    }
    
    handleChangeNota = (event) => {
        this.setState({
            nota: event.target.value
        });
    }
    
    gradeProject = () => {
        axios.put(`http://${localhost}:8080/users/${this.state.user.email}`, {nota_acordata: this.state.nota});
        this.props.history.push('/home', {user: this.state.user});
    }
    
    goToHome = () => {
        this.props.history.push('/home', {user: this.state.user});
    }
    
    render() {
        return(
            <div>
                <h1 className="welcomeHeader">Notare</h1>
                <div className="UserRowHome">
                    <p>User Curent: </p>
                    <input 
                        className="inputReadOnly"
                        type="text"
                        value={this.state.user.nume_utilizator}
                        readOnly/>
                </div>
                <div className="UserProjHome">
                    <p>Proiect Evaluat: </p>
                    <input
                        className="inputReadOnly"
                        type="text"
                        value={this.state.user.id_proiect_notat}
                        readOnly/>
                </div>
                <button id="getProjectButtonID" className="getProjectButton" onClick={this.seeDelivList}>Afiseaza livrabilele</button>
                <ol className="olList" id="delivListGrade"></ol>
                
                 <div className="UserProjHome">
                    <p>Nota: </p>
                    <input
                        className="inputReadWrite"
                        type="text"
                        value={this.state.nota}
                        onChange={this.handleChangeNota}
                        />
                </div>
                
                <button id="gradeButtonID" className="gradeButton" onClick={this.gradeProject}>Trimite</button>
                <button id="gradeButtonBackID" className="gradeButtonBack" onClick={this.goToHome}>Inapoi</button>
            </div>
            );
    }
}

export default GradeProject;