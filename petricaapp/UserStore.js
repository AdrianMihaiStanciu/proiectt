import React from 'react';


class UserStore extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: []
        };
    }
    
    componentDidUpdate = (prevProps) => {
        if(prevProps.users.length !== this.props.users.length) {
        
             this.setState({
            users: this.props.users
        })  
        }
    }

    render() {
        const users = this.state.users.map((users, email) => <div key={email}>{users.nume_utilizator}</div>)
        return (
            <div>
                <div>
                    {users}
                </div>
            </div>
        );
    }
}

export default UserStore;